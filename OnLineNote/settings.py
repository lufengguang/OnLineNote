# -*- coding: utf-8 -*- 
"""
Django settings for OnLineNote project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%2qmb+j9a07!$_ilu*rvd@5e=qw10*o-3r3zq8l!_e=pc!3fz9'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'note',
    'usercenter',
    'notepad',
    'mcode',
    'search',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'OnLineNote.urls'

WSGI_APPLICATION = 'OnLineNote.wsgi.application'


# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.sqlite3',
        #'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'letflygo',
        'USER':'root',
        'PASSWORD':'',
        'HOST':'127.0.0.1',
        'PORT':''
    }
}

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

EMAIL_HOST='smtp.qq.com' 
EMAIL_PORT=None 
EMIL_HOST_USER='82506507@qq.com' 
EMAIL_HOST_PASSWORD='lfg10864' 
EMAIL_USE_TLS=None 


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_ROOT =os.path.join(os.path.dirname(BASE_DIR),'static')

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),#此处必须，为新添加
)

STATIC_PATH= os.path.join(os.path.dirname(__file__), '../static').replace('\\','/')


TEMPLATE_DIRS = (
    #os.path.join(os.path.split(os.path.dirname(__file__))[0], 'static').replace('\\', '/'),
    (os.path.join(BASE_DIR,'static')),(os.path.join(BASE_DIR,'static/tpl/url')),
    (os.path.join(BASE_DIR,'static')),(os.path.join(BASE_DIR,'static/tpl/notepad')),
    (os.path.join(BASE_DIR,'static')),(os.path.join(BASE_DIR,'static/tpl/mcode')),
    (os.path.join(BASE_DIR,'static')),(os.path.join(BASE_DIR,'static/tpl/cau')),


)

