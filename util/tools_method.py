# -*- coding: utf-8 -*-




# 操作出现错误的响应函数
def failed_response(res={}):
    result = {
        'header': {},
        'msg': '系统繁忙，请稍后再试！',
        'res': API_ERRORS['common']['system_error'],
        'data': {}
    }
    if 'res' in res:
        mod, err = res['res'].split('.')
        res['res'] = API_ERRORS[mod][err]
    result.update(res)

    return HttpResponse(json.dumps(result))


# 操作成功的响应函数
def success_response(res={}):
    result = {
        'header': {},
        'msg': '操作成功！',
        'res': 0,
        'data': {}
    }
    result.update(res)

    return HttpResponse(json.dumps(result))
