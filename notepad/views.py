# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8') 
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from usercenter import views as user_views
from models import Note

def index(request):
	u=user_views.get_current_user(request)
	if u:
		ns=Note.objects.filter(user=u).order_by('-id')
		datalist=[n.serialize() for n in ns]
		return render(request,'tpl/notepad/index.html',{'user_name':u.name,'datalist':datalist})
	else:
		return HttpResponseRedirect('/login')
	pass

@csrf_exempt
def add_note(request):
	u=user_views.get_current_user(request)
	if u:
		if request.method == 'POST':
			msg=request.POST.get('msg',None)
			if msg and str(msg).strip()!=''  :
				note=Note()
				note.msg=msg
				note.user=u
				note.save()
				return HttpResponseRedirect('/note/index')
		else:
			return HttpResponseRedirect('/note/index')

	else:
		return HttpResponseRedirect('/login')

def delete_note(request):
	u=user_views.get_current_user(request)
	if u:
		if request.method == 'GET':
			id=request.GET.get('id',None)
			if id:
				note=Note.objects.get(id=id)
				note.delete()
				return HttpResponseRedirect('/note/index')
		else:
			pass

	else:
		return HttpResponseRedirect('/login')
