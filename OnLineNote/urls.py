from django.conf.urls import patterns, include, url
from django.contrib import admin
from note import views as note_views
admin.autodiscover()
from django.conf import settings
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'OnLineNote.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'search.views.cau_index'),
    url(r'^cau/index/$', 'search.views.cau_index'),
    url(r'^postgraduate/index/$', 'search.views.postgraduate'),
    url(r'^postgraduate/login/$', 'search.views.login'),
    url(r'^login/success/$', 'search.views.login_success'),
    url(r'^postgraduate/grade/$', 'search.views.grade'),


    url(r'^login/$', 'usercenter.views.login'),
    url(r'^logout/$', 'usercenter.views.logout'),
    url(r'^register/$', 'usercenter.views.register'),
    url(r'^url/index/$', 'note.views.get_urls'),
    url(r'^url/add/$', 'note.views.add_url'),
    url(r'^url/delete/$', 'note.views.delete_url'),
    url(r'^cate/add/$','note.views.add_category'),
    url(r'^note/index/$','notepad.views.index'),
    url(r'^note/add/$','notepad.views.add_note'),
    url(r'^note/del/$','notepad.views.delete_note'),
    url(r'^mcode/index/$','mcode.views.index'),
    url(r'^mcode/share/$','mcode.views.share'),
    url(r'^cau/s/$','search.views.search'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^site_media/(?P<path>.*)$','django.views.static.serve',{'document_root':settings.STATIC_PATH}),
)
