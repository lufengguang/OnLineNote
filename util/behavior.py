# -*- coding: utf-8 -*-
from django.db import models
import time
import datetime


class Serializable(models.Model):
    fields = []

    def serialize(self, fields=[]):
        '''
        1. 默认会读取字段输出
        2. 可以自定义配置字段
        3. 可以自定义字段的key
        '''
        fields = fields if fields else self.fields
        json = {}
        for f in fields:
            if (type(f) is str or type(f) is unicode) and '.' not in f:
                if hasattr(getattr(self, f), 'all'):
                    # 拥有多个关联，has_many and many_many
                    json[f] = [o.serialize() for o in getattr(self, f).all()]
                elif hasattr(getattr(self, f), 'serialize') and not hasattr(getattr(self, f), 'all'):
                    json[f] = getattr(self, f).serialize()
                else:
                    json[f] = self._get_serilizable_attr(f)
            elif type(f) is tuple:
                json[f[1]] = getattr(self, f[0])
            elif '.' in f:
                (relation_model, attr) = f.split('.')
        return json

    def _get_serilizable_attr(self, attr_name):
        field = getattr(self, attr_name)
        if type(field) is datetime.datetime:
            return str(field)
        elif type(field) is datetime.date:
            return str(field)
        return field

    class Meta:
        abstract = True

def __main__():

	pass