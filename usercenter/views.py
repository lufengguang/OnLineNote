# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.core.cache import cache
from django.contrib.auth.hashers import make_password, check_password
from models import User
import time
import hashlib
import random



def generate_sesstoken():
    '''生成sesstoken'''
    return hashlib.md5(str(time.time()) + str(random.randint(1111, 9999))).hexdigest()


def get_sesstoken(request):
    '''获得登录的token'''
    return request.META['HTTP_SESSTOKEN']

def index(request):
    print '2h'
    return render(request,'tpl/helloworld.html',{})

@csrf_exempt
def login(request):
    
    if request.method=='POST':
        email=request.POST['email']
        pwd=request.POST['pwd']
        us=User.objects.filter(email=email)
        if us:
            u=us[0]
            print  u.pwd
            print make_password(pwd)
            if check_password(pwd,u.pwd):
                print 'login success'
                '''
                user_info = u.serialize()
                sesstoken = generate_sesstoken()
                cache.set('session:' + sesstoken, user_info, 30 * 24 * 60 * 60)
                user_info.update({'sesstoken': sesstoken})
                '''
                request.session['user_id']=u.id
                return HttpResponseRedirect('/')
            else:
                return render(request,'tpl/login.html',{'err':"密码错误!"})
        else:
            return render(request,'tpl/login.html',{'err':"邮箱错误！"})
    return render(request,'tpl/login.html',{})


@csrf_exempt
def register(request):
    if request.method=='POST':
        email=request.POST['email']
        name=request.POST['name']
        print name
        pwd=request.POST['pwd']
        u=User.objects.filter(email=email)
        if u:
            return render(request,'tpl/register.html',{'err':"该邮箱已经被注册！"})
        else:
            u=User()
            u.email=email
            u.pwd=make_password(pwd)
            u.name=name
            u.save()
            return HttpResponseRedirect ('/login')           
    return render(request,'tpl/register.html',{})




def is_login(request):
    '''判断当前请求的用户是否登录，如果登录，返回当前的用户信息'''
    if get_user_cache_info(request):
        return True
    else:
        return False


def cache_user_info(user_info):
    sesstoken = generate_sesstoken()
    # 将用户信息保存到redis中，两个月
    cache.set('session:' + sesstoken, user_info, 30 * 24 * 60 * 60)
    return sesstoken


def get_user_cache_info(request):
    sesstoken = request.META.get('HTTP_SESSTOKEN', '')
    if sesstoken:
        return cache.get('session:' + sesstoken)
    else:
        return None



def get_current_user(request):
    if request.session.get('user_id',-1)!=-1:
        u=User.objects.get(id=request.session.get('user_id',-1))
        return u
    else:
     return None

def current_user(request=None):
    '''获取当前登录的用户'''
    if request is None:
        request = pandora.box['request']
    user_info = get_user_cache_info(request)
    if user_info:
        return User.objects.get(id=user_info['user_id'])
    else:
        return None


def logout(request):
    '''退出登录'''
    try:
        del request.session['user_id']
    except KeyError:
        pass
    return HttpResponseRedirect('/')
