# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.core.cache import cache
from django.contrib.auth.hashers import make_password, check_password
from models import Category,Url
from usercenter import views as user_views



def index(request):
	u=user_views.get_current_user(request)
	if u:
		return render(request,'tpl/index.html',{'user_name':u.name})
	else:
		return render(request,'tpl/index.html',{})



def get_urls(request):
	u=user_views.get_current_user(request)
	if u is not None:
		cs=Category.objects.filter(user=u)
		datalist=[]
		for i in range(0,len(cs)):
			c=cs[i]
			us=Url.objects.filter(category=c)
			ulist={'cate':c.serialize(),'u_list':[url.serialize() for url in us]}
			datalist.append(ulist)
		return render(request,'tpl/url/index.html',{'user_name':u.name,'datalist':datalist})
	else:
		return HttpResponseRedirect('/login')
@csrf_exempt
def add_url(request):
	u=user_views.get_current_user(request)
	if u is None:
		return HttpResponseRedirect('/login')
	if request.method=="POST":

		try:
			cate_id=request.POST["cate_id"]
			cate=Category.objects.get(id=cate_id)
			name=request.POST["name"]
			href=request.POST["href"]
			if name is None or name == ''  or href is None or href == '':
				return HttpResponseRedirect('/url/add')
			url=Url()
			url.category=cate
			url.name=name
			url.href=href
			url.save()
			return HttpResponseRedirect('/url/index')
		except Exception, e:
			return HttpResponseRedirect('/url/add')
		else:
			pass
		finally:
			pass
		return HttpResponseRedirect('/url/index')
	else:
		cs=Category.objects.filter(user=u)
		if len(cs)==0:
			c=Category()
			c.name="默认"
			c.user=u
			c.save()
			cs=[c]
		datalist=[c.serialize() for c in cs]
		print 'len',len(datalist)
		return render(request,'tpl/url/add_url.html',{'user_name':u.name,'datalist':datalist})

@csrf_exempt
def add_category(request):
	u=user_views.get_current_user(request)
	if u is None:
		return HttpResponseRedirect('/login')
	if request.method=="POST":
		name=request.POST['name']
		if name is None or name == '':
			return HttpResponseRedirect('/cate/add')
		else:
			cate=Category()
			cate.name=name
			cate.user=u
			cate.save()
			return HttpResponseRedirect('/url/index')
	else:
		cs=Category.objects.filter(user=u)
		if len(cs)==0:
			c=Category()
			c.name="默认"
			c.user=u
			c.save()
			cs=[c]
		datalist=[c.serialize() for c in cs]
		return render(request,'tpl/url/add_category.html',{'user_name':u.name,'datalist':datalist})

	return HttpResponseRedirect('/cate/add')

def delete_url(request):
	u=user_views.get_current_user(request)
	if u is None:
		return HttpResponseRedirect('/login')
	uid=request.GET.get('id',-1)
	print uid
	if uid is None or uid=='' or uid == -1:
		print 'err1'
		return HttpResponseRedirect('/url/index')
	url=Url.objects.get(id=uid)
	if url.category.user.id==u.id:
		url.delete()
	return HttpResponseRedirect('/url/index')
	pass
