# -*- coding: utf-8 -*-
from django.shortcuts import render
from usercenter import views as user_views
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from time import clock
from django.views.decorators.csrf import csrf_exempt

#from elasticsearch import Elasticsearch
from pyes import *
# Create your views here.




def cau_index(request):
    return render(request,'tpl/cau/cau_index.html',{})



def postgraduate(request):
    return render(request,'tpl/cau/postgraduate.html',{})


@csrf_exempt
def login(request):
    if request.method=="POST":
        return HttpResponseRedirect('/login/success/')
    else :
        return render(request,'tpl/cau/login.html',{})


def login_success(request):
    return render(request,'tpl/cau/master_index.html',{})


def grade(request):
    print 'grede'
    return render(request,'tpl/cau/login_success.html',{})


def index(request):
	u=user_views.get_current_user(request)
	if u:
		return render(request,'tpl/index.html',{'user_name':u.name})
	else:
		return render(request,'tpl/index.html',{})
def search(request):
    """docstring for search"""
    if 'keyword' in request.GET:
        q = request.GET['keyword']
        print 'k',q
        print q
        if 'page' in request.GET:
            page = unicode(request.GET['page'])
        else:
            page = unicode(1)
        start = clock()
        results = dosearch(q,page)#connect to ES to return the results
        end = clock()
        return render(request,'tpl/search/res_search.html', {'results' : results,
                                                    'query':q,
                                                    'count':len(results),
                                                    'time':end-start,
                                                    'page':page,
                                                    'nextpage':int(page)+1})
    else:
        message = 'You submitted an empty form.'
        return HttpResponse(message)

def dosearch(string,upage):
    conn = ES('127.0.0.1:9200', timeout=3.5)#连接ES
    #fq_title = FieldQuery(analyzer='jieba')
    #fq_title = FieldQuery()
    #fq_title.add('title',string)
 
    #fq_content = FieldQuery(analyzer='jieba')
    #fq_content = FieldQuery()
    #fq_content.add('content',string)
    q =query.MatchQuery('title',string,type="phrase_prefix")
 
    #bq = BoolQuery(should=[fq_title,fq_content])
 
    h=HighLighter(['['], [']'], fragment_size=100)
 
    page = int(upage.encode('utf-8'))
    if page < 1:
        page = 1
 
    s=Search(q,highlight=h,start=(page-1)*1024,size=1024)
    s.add_highlight("content")
    s.add_highlight('title')
    results=conn.search(s,indices='causpider',doc_types='searchEngine-type')
 
    list=[]
    for r in results:
        if(r._meta.highlight.has_key("title")):
            r['title']=r._meta.highlight[u"title"][0]
        if(r._meta.highlight.has_key('content')):
            r['content']=r._meta.highlight[u'content'][0]
 
        res = Results()
        res.content = r['content']
        res.title = r['title']
        print r['title']
        res.url = r['url']
        list.append(res)
    return list

class Results:

	def __init__(self):
		self.content=None
		self.title=None
		self.url=None

