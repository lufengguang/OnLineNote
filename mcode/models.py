# -*- coding: utf-8 -*-
from django.db import models
from usercenter.models import User
from util.behavior import Serializable



class MCode(Serializable):
    fields = ['id','mcode']
    mcode=models.TextField(blank=True, null=True ,default='未设置')
    user = models.ForeignKey('usercenter.User', blank=True, null=True,default='',db_constraint=False)
    #db_constraint=False 没有外键不报错
    def __unicode__(self):
      return self.id
 