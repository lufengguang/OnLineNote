# -*- coding: utf-8 -*-
from django.db import models
from util.behavior import Serializable


class User(Serializable):
    fields = [('id','user_id'),'email','name','pwd']
    email = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    pwd = models.CharField(max_length=255)
    
    def __unicode__(self):
      return self.id

