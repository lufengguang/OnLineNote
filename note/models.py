# -*- coding: utf-8 -*-
from django.db import models
from usercenter.models import User
from util.behavior import Serializable

class Url(Serializable):
    fields = ['id','href','name']
    category=models.ForeignKey('note.Category',blank=False,null=True)
    name=models.CharField(max_length=255,blank=True,null=True,default='')
    href=models.CharField(max_length=255,blank=True,null=True,default='')
    def __unicode__(self):
      return self.id

class Category(Serializable):
    fields = ['id','name']
    name=models.CharField(max_length=255, blank=True, null=True ,default='未设置')
    user = models.ForeignKey('usercenter.User', blank=True, null=True,default='')
    #db_constraint=False 没有外键不报错
    def __unicode__(self):
      return self.id
 