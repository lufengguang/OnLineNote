from django.shortcuts import render
from usercenter import views as user_views
from django.http import HttpResponseRedirect
from mcode.models import MCode
from django.views.decorators.csrf import csrf_exempt


def index(request):
	u=user_views.get_current_user(request)
	if u:
		return render(request,'tpl/mcode/index.html',{'user_name':u.name,'user_id':u.id})
	else:
		return render(request,'tpl/mcode/index.html',{})
	pass

@csrf_exempt
def share(request):
	if request.method=="POST":
		user_id=request.POST.get('user_id',-1)
		mcode=request.POST.get('mcode','')
		print mcode,'mc'
		if mcode.strip()=='':
			return HttpResponseRedirect('/mcode/index/')
		if user_id and user_id != '':
			u=user_views.get_current_user(request)
		else:
			u=None
		code=MCode()
		code.mcode=mcode
		code.user=u
		code.save()
		return HttpResponseRedirect('/mcode/share/?id='+str(code.id))
	else:
		code_id=request.GET.get('id',-1)
		if code_id==-1 or code_id=='':
			return HttpResponseRedirect('/mcode/index/')
		code=MCode.objects.get(id=code_id)
		return render(request,'tpl/mcode/mcode.html',{'code':code.mcode,'url':'http://www.letflygo.cn/mcode/share/?id='+str(code_id)})
