# -*- coding: utf-8 -*-
from django.db import models
from usercenter.models import User
from util.behavior import Serializable

class Note(Serializable):
    fields = ['id','msg']
    msg=models.TextField(blank=True,null=True,default='')
    user = models.ForeignKey('usercenter.User', blank=True, null=True,default='')
    def __unicode__(self):
      return self.id
