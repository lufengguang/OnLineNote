"""
WSGI config for OnLineNote project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/dev/howto/deployment/wsgi/
"""

import os
import sys
sys.path.append('/usr/local/lib/python2.7/site-packages/')
sys.path.append('/usr/local/lib/python2.7/site-packages')
#sys.path.append('/srv/www/OnLineNote/OnLineNote')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "OnLineNote.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
