//JS模拟Marquee连续滚屏效果
//IE 6.0 测试通过
//FireFox 1.5.0.7 测试通过
//Opera 9.02 测试通过
//NetScape 8.1测试通过

//author:梦猫猫
//QQ:16991200
//Created:2006-10-18
//intro:
/*
  instance:创建的实例名称
  marquee:滚动区域容器的ID，要求无填充，无边框，overflow为hidden
  direction:滚动方向，值分别为：up,down,left,right
  delay:滚动的速度，值最小为1，值越小越快
  amount:滚动步进量，值最小为1，值越大越快
*/


 function Marquee(instance,marquee,direction,delay,amount){
  this.instance =instance;//实例名称
  this.marquee = document.getElementById(marquee);
  this.delay = delay;
  if(this.delay==null){
   this.delay = 1;
  }
  this.amount = amount;
  if(this.amount==null){
   this.amount = 1;
  }
  this.direction = direction;
  this.width = this.marquee.clientWidth;
  this.height = this.marquee.clientHeight;
  this.container = document.createElement("table");
  this.container.border=0;
  this.container.cellspacing="0"
  this.container.cellpadding="0"
  var _HTML = this.marquee.innerHTML;
  this.marquee.innerHTML = "";
  this.marquee.appendChild(this.container);
  var _tr = this.container.insertRow(this.container.rows.length);
  var _td = _tr.insertCell(_tr.cells.length);
  _td.innerHTML = _HTML;
  this.scrollHeight = this.container.clientHeight;
  this.scrollWidth = this.container.clientWidth;
  switch(this.direction.toLowerCase()){
   case "up":
   case "down":
    if(this.scrollHeight>=this.height){
     var _tr = this.container.insertRow(this.container.rows.length);
     var _td = _tr.insertCell(_tr.cells.length);
     _td.innerHTML = _HTML;
     this.scrollHeight = this.container.clientHeight;
    }
    if(this.direction.toLowerCase()=="down"){
     this.marquee.scrollTop=this.scrollHeight/2;
    }
    break;
   default:
    if(this.scrollWidth>=this.width){
     var _td = _tr.insertCell(_tr.cells.length);
     _td.innerHTML = _HTML;
     this.scrollWidth = this.container.clientWidth;
    }

  }

  this.marquee.m = this;

  this.intervalId = null;
  this.marquee.onmouseover = function (){
   this.m.Stop();
  }
  this.marquee.onmouseout = function (){
   this.m.Start();
  }
  this.Start();
 }
 Marquee.prototype.Start = function (){
  //alert(this.instance);
  this.intervalId = setInterval(this.instance+".Scroll()",this.delay)
  //this.Scroll();
 }
 Marquee.prototype.Stop = function (sender){
  if(this.intervalId!=null){
   clearInterval(this.intervalId);
  }
 }
 Marquee.prototype.Scroll = function (){
  switch(this.direction.toLowerCase()){
   case "up":
    if((this.marquee.scrollTop+this.height)>=this.scrollHeight){
     this.marquee.scrollTop-=this.scrollHeight/2
    }
    if(this.scrollHeight>this.height){
     this.marquee.scrollTop+=this.amount;
    }
    break;
   case "down":
    if(this.marquee.scrollTop<=0){
     this.marquee.scrollTop+=this.scrollHeight/2
    }
    if(this.scrollHeight>this.height){
     this.marquee.scrollTop-=this.amount;
    }
    break;
   case "right":
    if(this.marquee.scrollLeft<=0){
     this.marquee.scrollLeft+=this.scrollWidth/2
    }
    if(this.scrollWidth>this.width){
     this.marquee.scrollLeft-=this.amount;
    }
    break;
   default:
    if((this.marquee.scrollLeft+this.width)>=this.scrollWidth){
     this.marquee.scrollLeft-=this.scrollWidth/2
    }
    if(this.scrollWidth>this.width){
     this.marquee.scrollLeft+=this.amount;
    }
  }
 }

var Marquee_array = new Array();
var i = 0;
function Add_array(str)
{
	var j = 0;
    Marquee_array[i++]=str;
}

function Create_marquee()
{
    for(var j=0;j<Marquee_array.length;j++)
    {
        //alert(Marquee_array[j]);
        //eval(Marquee_array[j]);
        //eval(' var _m = new Marquee("_m","marquee1092","left",50,1)');
    }
    //var _m =  new Marquee("_m","marquee1092","left",50,1);
}


